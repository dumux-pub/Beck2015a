Summary

===============================

This is the DuMuX module containing the code for producing the results



Installation
============
The easiest way to install this module is to create a new folder and to execute the file
[installBeck2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Beck2015a/raw/master/installBeck2015a.sh)
in this folder.

```bash
mkdir -p Beck2015a && cd Beck2015a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Beck2015a/raw/master/installBeck2015a.sh
sh ./installBeck2015a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============
The applications used for this publication can be found in test/geomechanics/viscoel2p
In order to run an executable, type e.g.:
```bash
cd dumux-Beck2015a/test/geomechanics/viscoel2p
make test_viscoel2pforce
./test_viscoel2pforce -ParameterFile test_viscoel2pforce.input 
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installBeck2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Beck2015a/raw/master/installBeck2015a.sh).

In addition the following external software package are necessary for
compiling the executables:

| software           | version | type          |
| ------------------ | ------- | ------------- |
| cmake              | 3.5.2   | build tool    |
| SuperLU            | -       | linear solver |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| clang/clang++ | 3.5     |
| gcc/g++       | 4.7     |


