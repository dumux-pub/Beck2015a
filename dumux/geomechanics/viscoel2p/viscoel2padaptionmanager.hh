#ifndef DUMUX_VISCOELASTIC2P_ADAPTION_MANAGER_HH
#define DUMUX_VISCOELASTIC2P_ADAPTION_MANAGER_HH

#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/adaptivity/adaptivity.hh>
#include <dumux/geomechanics/viscoel2p/pdelabcommon/pdelabadaptionmanager.hh>


namespace Dumux
{

namespace PDELab
{
  template <typename TypeTag>
  class ViscoElTwoPAdaptionManager : public PDELabAdaptionManager<TypeTag>
    {
  private:
        typedef PDELabAdaptionManager<TypeTag> ParentType;
        typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
        typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
        typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
        typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
        typedef typename GET_PROP_TYPE(TypeTag, PTAG(GridFunctionSpace)) GridFunctionSpace;
        typedef Dune::PDELab::LocalFunctionSpace<GridFunctionSpace> LocalFunctionSpace;
        enum{
          dim = GridView::dimension,
          dimWorld = GridView::dimensionworld
        };
        typedef typename Grid::ctype coord;
        typedef typename Problem::AdaptedValues AdaptedValues;
        typedef Dune::PersistentContainer<Grid, AdaptedValues> PersistentContainer;
        typedef typename Grid::LevelGridView LevelGridView;
        typedef typename LevelGridView::template Codim<0>::Iterator LevelIterator;
        typedef typename GridView::Traits::template Codim<0>::Entity Element;
        typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
        typedef typename GridView::Traits::template Codim<dim>::Entity Vertex;
        typedef typename GridView::Traits::template Codim<dim>::EntityPointer VertexPointer;

        typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
        typedef Dune::FieldVector<coord, dim> LocalPosition;

        PersistentContainer adaptationMap_;
        Scalar eps_ = 3e-6;
        int refinementLevel_;

  public:
    ViscoElTwoPAdaptionManager(Problem& problem)
        : ParentType(problem), adaptationMap_(problem.grid(), dim)
    {}

    ViscoElTwoPAdaptionManager(Problem& problem, Scalar alpha_frac, Scalar beta_frac, bool elementFrac, int degree, int verbos)
    : ParentType(problem, alpha_frac, beta_frac, elementFrac, degree, verbos), adaptationMap_(problem.grid(), dim)
    {}

    void storeVariables()
    {
      refinementLevel_ = this->problem_.grid().maxLevel();
      std::cout << "Storing of non-primary variables... ";
      adaptationMap_.resize();

        // loop over all levels of the grid
        for (int level = this->problem_.grid().maxLevel(); level >= 0; level--)
        {
            //get grid view on level grid
            LevelGridView levelView = this->problem_.grid().levelGridView(level);

            for (LevelIterator eIt = levelView.template begin<0>(); eIt != levelView.template end<0>(); ++eIt)
            {
                if (eIt->isLeaf())
                {
                    // Loop over the vertices of the element to store data associated with the vertices
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                    for (int vertIdx = 0; vertIdx < eIt->subEntities(dim); vertIdx++)
#else
                    for (int vertIdx = 0; vertIdx < eIt->template count<dim>(); vertIdx++)
#endif
                    {
                        VertexPointer vertex = eIt->template subEntity<dim>(vertIdx);
                        //get your map entry
                        AdaptedValues &adaptedValues = adaptationMap_[*vertex];

                        // If we have set the value for this node already, skip the rest
                        if(!adaptedValues.valueSet)
                        {
                            // get global Index
                            int globalIdx = this->problem_.vertexMapper().map(*vertex);
                            // store pInit in adaptedValues container
                            adaptedValues.pInit = this->problem_.pInit()[globalIdx];
                            adaptedValues.valueSet = true;
                        }
                    }
                }
                //Copy and interpolate values in father
                if (eIt->level() > 0)
                {
                    ElementPointer fatherElement = eIt->father();
                    bool levelIsFiner = eIt->geometry().volume() < fatherElement->geometry().volume() ? true : false;

                    // Loop over the vertices of the father element to store data associated with the vertices
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                    for (int vertIdx = 0; vertIdx < fatherElement->subEntities(dim); vertIdx++)
#else
                    for (int vertIdx = 0; vertIdx < fatherElement->template count<dim>(); vertIdx++)
#endif
                    {
                        VertexPointer vertexFather = fatherElement->template subEntity<dim>(vertIdx);
                        AdaptedValues &adaptedValuesFather = adaptationMap_[*vertexFather];
                        // If we have set the value for this node already, skip the rest
                        if(!adaptedValuesFather.valueSet)
                        {
                            // Check if Vertex exists in actual Element and if so, set value
                            int actualIndex;
                            bool hasVertex = checkElementForVertex(*vertexFather, *eIt, actualIndex);
                            if(hasVertex == true)
                            {
                                //std::cout << "ActualIndex: " << actualIndex << std::endl;
                                VertexPointer actualVertex = eIt->template subEntity<dim>(actualIndex);
                                AdaptedValues &adaptedValues = adaptationMap_[*actualVertex];
                                adaptedValuesFather.pInit = adaptedValues.pInit;
                            }
                            // If the above level is finer, we have to interpolate the Values to the additional nodes
                            if(levelIsFiner == false && hasVertex == false)
                            {
                                GlobalPosition fatherVertPos = vertexFather->geometry().corner(0);
                                LocalPosition fatherVertPosLocal = eIt->geometry().local(fatherVertPos);

                                // get local function space for the interpolation of the nodal values
                                LocalFunctionSpace localfunctionspace(this->problem_.model().jacobianAssembler().gridFunctionSpace());
                                localfunctionspace.bind(*eIt);
                                typedef typename LocalFunctionSpace::template Child<0>::Type PressSatFEM;
                                const PressSatFEM& pressSatFEM = localfunctionspace.template child<0>();
                                typedef typename PressSatFEM::template Child<0>::Type PressureFEM;
                                const PressureFEM& pressureFEM = pressSatFEM.template child<0>();
                                typedef Dune::FieldVector<coord, 1> shapeVal;
                                std::vector<shapeVal> shapeValues;
                                pressureFEM.finiteElement().localBasis().evaluateFunction(fatherVertPosLocal, shapeValues);

                                Scalar pInitvalue = 0;

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                                for (int actualVertIdx = 0; actualVertIdx < eIt->subEntities(dim); actualVertIdx++)
#else
                                for (int actualVertIdx = 0; actualVertIdx < eIt->template count<dim>(); actualVertIdx++)
#endif
                                {
                                    VertexPointer actualVertEntity = eIt->template subEntity<dim>(actualVertIdx);
                                    AdaptedValues &adaptedValues = adaptationMap_[*actualVertEntity];
                                    pInitvalue += adaptedValues.pInit*shapeValues[actualVertIdx];
                                }
                                adaptedValuesFather.pInit = pInitvalue;
                                adaptedValuesFather.valueSet = true;
                            }
                        }
                    }
                }
            }
        }

        std::cout << "done." << std::endl;
    }

    bool checkElementForVertex(Vertex &vertex, Element& element, int& actualIndex)
    {
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int numVert = element.subEntities(dim);
#else
        int numVert = element.template count<dim>();
#endif
        for(int vIdx = 0; vIdx < numVert; vIdx++)
        {
            //Vertex vertexElement = element.template subEntity<dim>(vIdx);
            //GlobalPosition ElemVertPos = vertexElement->geometry().corner(0);
            GlobalPosition ElemVertPos = element.template subEntity<dim>(vIdx)->geometry().corner(0);
            GlobalPosition VertexPos = vertex.geometry().corner(0);
            if(ElemVertPos[0] > VertexPos[0] - eps_ && ElemVertPos[0] < VertexPos[0] + eps_ &&
               ElemVertPos[1] > VertexPos[1] - eps_ && ElemVertPos[1] < VertexPos[1] + eps_ &&
               ElemVertPos[2] > VertexPos[2] - eps_ && ElemVertPos[2] < VertexPos[2] + eps_)
            {
                actualIndex = vIdx;
                return true;
            }
        }
        return false;
    }

    void reconstructVariables()
    {
        std::cout << "Reconstruction of non-primary variables... ";

        adaptationMap_.resize();
        // temporary object to store pInit
        std::vector<Scalar> pInitTemp(this->problem_.gridView().size(dim));

        for (int level = 0; level <= this->problem_.grid().maxLevel(); level++)
        {
            LevelGridView levelView = this->problem_.grid().levelGridView(level);

            for (LevelIterator eIt = levelView.template begin<0>(); eIt != levelView.template end<0>(); ++eIt)
            {
                // only treat non-ghosts, ghost data is communicated afterwards
                if (eIt->partitionType() == Dune::GhostEntity)
                    continue;

                if (eIt->level() <= refinementLevel_)
                {
                    //entry is in map, write in leaf
                    if (eIt->isLeaf())
                    {
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                        for (int vertIdx = 0; vertIdx < eIt->subEntities(dim); vertIdx++)
#else
                        for (int vertIdx = 0; vertIdx < eIt->template count<dim>(); vertIdx++)
#endif
                        {
                            VertexPointer vertex = eIt->template subEntity<dim>(vertIdx);
                            AdaptedValues &adaptedValues = adaptationMap_[*vertex];
                            if(!adaptedValues.valueCaught)
                            {
                                int newIndex = this->problem_.vertexMapper().map(*vertex);
                                pInitTemp[newIndex] = adaptedValues.pInit;
                                adaptedValues.valueCaught = true;
                            }
                        }
                    }
                }
                else
                {
                    // value is not in map, interpolate from father element
                    if (eIt->level() > 0)
                    {
                        ElementPointer epFather = eIt->father();
                        bool levelIsFiner = eIt->geometry().volume() < epFather->geometry().volume() ? true : false;

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                        for (int vertIdx = 0; vertIdx < eIt->subEntities(dim); vertIdx++)
#else
                        for (int vertIdx = 0; vertIdx < eIt->template count<dim>(); vertIdx++)
#endif
                        {
                            VertexPointer vertex = eIt->template subEntity<dim>(vertIdx);
                            AdaptedValues &adaptedValues = adaptationMap_[*vertex];
                            if (!adaptedValues.valueSet)
                            {
                                // Check if Vertex exists also in father Element and if so, set value
                                int actualIndex;
                                bool hasVertex = checkElementForVertex(*vertex, *epFather, actualIndex);
                                if(hasVertex == true)
                                {
                                    VertexPointer fatherVertex = epFather->template subEntity<dim>(actualIndex);
                                    AdaptedValues &adaptedValuesFather = adaptationMap_[*fatherVertex];
                                    adaptedValues.pInit = adaptedValuesFather.pInit;
                                    adaptedValues.valueSet = true;
                                }
                                // If the new level is finer, we have to interpolate the Values to the additional nodes
                                if(levelIsFiner == true && hasVertex == false)
                                {
                                    GlobalPosition vertPos = vertex->geometry().corner(0);
                                    LocalPosition vertPosLocal = epFather->geometry().local(vertPos);

                                    // get local function space for the interpolation of the nodal values
                                    LocalFunctionSpace localfunctionspace(this->problem_.model().jacobianAssembler().gridFunctionSpace());
                                    localfunctionspace.bind(*epFather);
                                    typedef typename LocalFunctionSpace::template Child<0>::Type PressSatFEM;
                                    const PressSatFEM& pressSatFEM = localfunctionspace.template child<0>();
                                    typedef typename PressSatFEM::template Child<0>::Type PressureFEM;
                                    const PressureFEM& pressureFEM = pressSatFEM.template child<0>();
                                    typedef Dune::FieldVector<coord, 1> shapeVal;
                                    std::vector<shapeVal> shapeValues;
                                    pressureFEM.finiteElement().localBasis().evaluateFunction(vertPosLocal, shapeValues);

                                    Scalar pInitvalue = 0;

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                                    for (int fatherVertIdx = 0; fatherVertIdx < epFather->subEntities(dim); fatherVertIdx++)
#else
                                    for (int fatherVertIdx = 0; fatherVertIdx < epFather->template count<dim>(); fatherVertIdx++)
#endif
                                    {
                                        VertexPointer fatherVertEntity = epFather->template subEntity<dim>(fatherVertIdx);
                                        AdaptedValues &adaptedValuesFather = adaptationMap_[*fatherVertEntity];
                                        pInitvalue += adaptedValuesFather.pInit*shapeValues[fatherVertIdx];
                                    }
                                    adaptedValues.pInit = pInitvalue;
                                    adaptedValues.valueSet = true;
                                }
                            }
                        }

                        // if we are on leaf, store reconstructed values
                        if (eIt->isLeaf())
                        {
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                            for (int vertIdx = 0; vertIdx < eIt->subEntities(dim); vertIdx++)
#else
                            for (int vertIdx = 0; vertIdx < eIt->template count<dim>(); vertIdx++)
#endif
                            {
                                VertexPointer vertex = eIt->template subEntity<dim>(vertIdx);
                                AdaptedValues &adaptedValues = adaptationMap_[*vertex];
                                if(!adaptedValues.valueCaught)
                                {
                                    int newIndex = this->problem_.vertexMapper().map(*vertex);
                                    pInitTemp[newIndex] = adaptedValues.pInit;
                                    adaptedValues.valueCaught = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        // reset entries in restrictionmap
        adaptationMap_.resize( typename PersistentContainer::Value() );
        adaptationMap_.shrinkToFit();
        adaptationMap_.fill( typename PersistentContainer::Value() );

        this->problem_.passNewPinit(pInitTemp);
        std::cout << "done." << std::endl;
    }

    void updateAfterAdaption()
    {
      std::cout << "Adapting the local variables in the model..." << std::endl;
      // Now we adjust the private variables used in the model...
      this->problem_.model().updateAfterAdaption();
      // Adapt Matrix size in the assembler
      this->problem_.model().jacobianAssembler().redefineMatrix();

      // Evaluate the shear failure criterion again
      std::cout << "Evaluating pcrShear again..." << std::endl;
      this->problem_.model().evaluatePcrshe(this->problem_.model().curSol());
    }

    bool adaptGrid()
    {
        bool adapted = false;
        // Make finite element map
        typename ParentType::P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::simplex,dim));
        // Make the grid function space for the error indicator
        typename ParentType::P0GFS p0gfs(this->problem_.model().gridView(),p0fem);
        // Make the grid operator of the error indicator
        typename ParentType::ADAPTGO adaptgo(this->problem_.model().jacobianAssembler().gridFunctionSpace(),p0gfs,this->adapIndicator_);

        // DOF vector containing the local error indicators
        typename ParentType::U0 adap(p0gfs,0.0);
        adaptgo.residual(this->problem_.model().curSol(),adap);

        if (this->verbosity_ > 2)
        {
          std::cout << "Printing error vector after evaluating the indicator: " << std::endl;
          for(int i = 0; i < adap.N();i++)
            std::cout << adap.base()[i] << std::endl;
        }

        storeVariables();

        // Input variables to the element fraction method
        double eta_beta(0);          // coarsening threshold
        double eta_alpha(0);          // refinement threshold

        Dune::PDELab::element_fraction( adap, this->alpha_fraction_, this->beta_fraction_, eta_alpha, eta_beta, this->verbosity_);

        if(eta_alpha == 0)
          // That means that the global indicator vector is still zero on every entry,
          // we don't want to refine then and set eta_alpha to a value > 0 so that no entity will be marked
          eta_alpha = 1;

        // We don't want coarsening to happen at the moment, but it could be introduced later
        // The indicator would have to be adjusted
        eta_beta = -1;

        Dune::PDELab::mark_grid( this->problem_.grid(), adap, eta_alpha, eta_beta, this->min_level_, this->max_level_, this->verbosity_); // coarsening is switched off!
        Dune::PDELab::adapt_grid( this->problem_.grid(), this->problem_.model().jacobianAssembler().gridFunctionSpace(),
                        this->problem_.model().curSol(), this->problem_.model().prevSol(), this->degree_ );

        if (eta_alpha != 1)
        {
            this->updateMappers();
            reconstructVariables();
            updateAfterAdaption();
            adapted = true;
        }

        return adapted;
    }
  };
}
}

#endif